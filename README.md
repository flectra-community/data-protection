# Flectra Community / data-protection

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[privacy_consent](privacy_consent/) | 2.0.1.0.1| Allow people to explicitly accept or reject inclusion in some activity, GDPR compliant
[privacy](privacy/) | 2.0.1.0.0| Provides data privacy and protection features to comply to regulations, such as GDPR.


